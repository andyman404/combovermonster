﻿using UnityEngine;
using System.Collections;

public class RandomBehaviour : StateMachineBehaviour {
	public string parameter = "tiltX";

	public float minInterval = 1.0f;
	public float maxInterval = 4.0f;

	public float minRange = -1.0f;
	public float maxRange = 1.0f;

	private float nextTime = 0.0f;
	private float startTime = 0.0f;
	private float intervalDuration = 1.0f;

	private float target = 0.0f;
	private float source = 0.0f;

//	 // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
//	override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
//	}

	// OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
	override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
		if (Time.time > nextTime)
		{
			source = animator.GetFloat (parameter);
			target = Random.Range(minRange, maxRange);
			startTime = Time.time;
			intervalDuration = Random.Range (minInterval, maxInterval) + 0.01f;
			nextTime = startTime + intervalDuration;

		}

		float current = Mathf.Lerp (source, target, (Time.time - startTime) / intervalDuration);
		animator.SetFloat (parameter, current);
	}

	// OnStateExit is called when a transition ends and the state machine finishes evaluating this state
	//override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}

	// OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
	//override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}

	// OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
	//override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}
}
