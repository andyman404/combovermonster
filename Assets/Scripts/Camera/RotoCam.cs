﻿using UnityEngine;
using System.Collections;

public class RotoCam : MonoBehaviour {
	public Transform target;
	public Vector3 focalOffset;

	public float distance = 5.0f;
	public float minDistance = 2.0f;
	public float maxDistance = 10.0f;
	public float cameraSensitivity = 90.0f;
	public float cameraLerpRate = 3.0f;
	public float zoom = 0.5f;
	public float minFOV = 45.0f;
	public float maxFOV = 90.0f;
	public float fov = 60.0f;
	public float zoomSensitivity = 0.1f;

	public bool useFixedOffset;
	public Vector3 fixedOffsetMin;
	public Vector3 fixedOffsetMax;

	public bool lockCursor = true;

	public float facingMultiplier;

	public float offsetMultiplier = 1.0f;

	private Vector3 focalPosition;
	private Quaternion positionRotation;
	private Quaternion targetPositionRotation;
	private int skipUpdates = 3;
	private Camera myCamera;
	private Vector3 oldForward;
	// Use this for initialization
	void Start () {
		focalPosition = target.position + focalOffset;
		positionRotation = Quaternion.LookRotation (transform.position - focalPosition);
		myCamera = GetComponent<Camera>();

		UpdateCursorLock ();
	}

	void UpdateCursorLock ()
	{
		if (lockCursor) {
			Cursor.visible = false;
			Cursor.lockState = CursorLockMode.Locked;
		}
		else {
			Cursor.visible = true;
			Cursor.lockState = CursorLockMode.None;
		}
	}
	
	// Update is called once per frame
	void Update () {
		UpdateCursorLock ();

		if (skipUpdates > 0)
		{
			skipUpdates--;
			return;
		}


		float scroller = Input.GetAxis ("Mouse ScrollWheel");
		zoom += scroller * zoomSensitivity;
		zoom = Mathf.Clamp01(zoom);
		float newFOV = Mathf.Lerp (minFOV, maxFOV, zoom);
		fov = Mathf.Lerp (fov, newFOV, cameraLerpRate * Time.deltaTime);
		myCamera.fieldOfView = fov;

		float newDistance = Mathf.Lerp (minDistance, maxDistance, zoom);
		distance = Mathf.Lerp (distance, newDistance, cameraLerpRate * Time.deltaTime);

		if (useFixedOffset)
		{
			Vector3 forward = Vector3.Lerp (oldForward, target.forward, 1f * Time.deltaTime);
			oldForward = forward;
			focalPosition = target.position + focalOffset + facingMultiplier * forward + Vector3.right * -4.0f;
			Vector3 offset = Vector3.Lerp (fixedOffsetMin, fixedOffsetMax, zoom) * offsetMultiplier;
			transform.position = focalPosition + offset;
			transform.LookAt (focalPosition);
		}
		else
		{
			Vector3 forwardVector = targetPositionRotation * Vector3.forward;
			targetPositionRotation = Quaternion.LookRotation (forwardVector, Vector3.up);		
			targetPositionRotation *= Quaternion.AngleAxis((Input.GetAxis ("Mouse X")) * cameraSensitivity, Vector3.up);
			targetPositionRotation *= Quaternion.AngleAxis((Input.GetAxis ("Mouse Y")) * cameraSensitivity, Vector3.left);

	//		focalPosition = Vector3.Lerp (focalPosition, target.position + focalOffset, 10.0f * Time.deltaTime);
			focalPosition = target.position + focalOffset;

			positionRotation = Quaternion.Slerp (positionRotation, targetPositionRotation, cameraLerpRate * Time.unscaledDeltaTime);
	//		positionRotation = targetPositionRotation;
			transform.position = focalPosition + positionRotation * Vector3.back * distance;

			Vector3 diff = focalPosition - transform.position;
			transform.rotation = Quaternion.LookRotation (diff);
			}
	}
}
