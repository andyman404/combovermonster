﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(CharacterMover))]
[RequireComponent(typeof(Rigidbody))]
public class Candidate : MonoBehaviour {

	public string tweetKey;

	public float stopDist = 3.0f;

	public Vector2 tweetInterval = new Vector2(8.0f,12.0f);

	private Vector3 targetPosition;
	private Vector3 moveDir;
	private float push;
	private float nextCheckTime = 0.0f;
	private Vector2 checkInterval = new Vector2(3.0f, 6.0f);
	private Rigidbody rb;
	private CharacterMover mover;
	private TweetShooter tweetShooter;
	private float nextTweetTime = 0.0f;

	void Awake() {
		mover = GetComponent<CharacterMover>();
		rb = GetComponent<Rigidbody>();
		tweetShooter = GetComponent<TweetShooter>();
	}
	// Use this for initialization
	void Start () {
	
	}


	// Update is called once per frame
	void Update () {
		if (Time.time > nextCheckTime)
		{
			nextCheckTime = Time.time + Random.Range (checkInterval.x, checkInterval.y);
			targetPosition = transform.position 
				+ new Vector3(
					Random.Range (-20.0f, 20.0f),
					0.0f,
					Random.Range (-20.0f, 20.0f));
		}

		Vector3 diff = targetPosition - transform.position;
		float dist = diff.magnitude;
		moveDir = diff.normalized;
		moveDir.y = 0.0f;
		moveDir.Normalize();

		push = dist > stopDist ? 1.0f : 0.0f;

		if (Time.time > nextTweetTime)
		{
			nextTweetTime = Time.time + Random.Range (tweetInterval.x, tweetInterval.y);
			tweetShooter.FireTweet(tweetKey);
		}
	}

	void FixedUpdate() {
		if (mover != null && rb != null)
		{
			mover.Move (moveDir, push);
		}
	}
	
}
