﻿using UnityEngine;
using System.Collections;

public class CharacterMover : MonoBehaviour {

	public float maxSpeed = 4.0f;

	public float acceleration = 8.0f;
	public float deceleration = 20.0f;

	public float turnRate = 360.0f;

	public float currentSpeed = 0;
	public Animator anim;
	private Rigidbody rb;
	
	void Awake() {
		rb = GetComponent<Rigidbody>();	
	}
	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		if (anim != null)
		{
			anim.SetFloat ("Speed", currentSpeed, 0.1f, Time.deltaTime);
		}
	}

	public void Move(Vector3 dir, float push)
	{
		// handle velocity

		dir.y = 0.0f;
		dir = dir.normalized;

		float targetSpeed = maxSpeed * push;

		if (targetSpeed > currentSpeed)
		{
			currentSpeed = Mathf.MoveTowards (currentSpeed, targetSpeed, acceleration * Time.deltaTime);
		}
		else
		{
			currentSpeed = Mathf.MoveTowards (currentSpeed, 0.0f, deceleration * Time.deltaTime);
		}

		if (push <= 0.01f)
		{
			dir = rb.rotation * Vector3.forward;
		}
		Vector3 v = rb.velocity;


		v.x = dir.x * currentSpeed;
		v.z = dir.z * currentSpeed;

		rb.velocity = v;

		// handle rotation

		if (push > 0.01f)
		{
			Quaternion targetRot = Quaternion.LookRotation (dir);
			rb.rotation = Quaternion.RotateTowards(rb.rotation, targetRot, turnRate * Time.deltaTime);
		}
	}

}
