﻿using UnityEngine;
using System.Collections;

public class DonaldTransformer : MonoBehaviour {

	public float comboverPower = 0.0f;
	public float maxComboverPower = 100.0f;
	public float comboverRechargeRate = 10.0f;
	public float comboverDischargeRate = 20.0f;

	public SkinnedMeshRenderer headMeshRenderer;
	public SkinnedMeshRenderer comboverMeshRenderer;
	public SkinnedMeshRenderer monsterMeshRenderer;

	[Range(0.0f, 1.0f)]
	public float monster = 0.0f;

	public float normalSize = 1.0f;
	public float monsterSize = 3.0f;

	public Material comboverMaterial;


	public RotoCam rotoCam;
	public float normalRotoCamDistance = 2.0f;
	public float monsterRotoCamDistance = 10.0f;
	public Animator anim;
	public ParticleSystem hairParticleSystem;
	public float monsterChangeRate = 2.0f;

	public Material eatenVoterMaterial;

	private TweetShooter tweetShooter;

	private float currentMonster = 0.0f;
	void Awake() {
		tweetShooter = GetComponent<TweetShooter>();
	}
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKey(KeyCode.LeftShift) || Input.GetKey (KeyCode.RightShift))
		{
			monster = 1.0f;
		}
		else
		{
			monster = 0.0f;
		}

		// recharge
		if (monster <= 0.5f)
		{
			comboverPower += comboverRechargeRate * Time.deltaTime;
		}
		else
		{
			comboverPower -= comboverDischargeRate * Time.deltaTime;
		}

		if (comboverPower <= 0.0f)
		{
			monster = 0.0f;
			comboverPower = 0.0f;
		}
		if (comboverPower >= maxComboverPower)
		{
			comboverPower = maxComboverPower;
		}

		currentMonster = Mathf.MoveTowards (currentMonster, monster, monsterChangeRate * Time.deltaTime);
//		currentMonster = Mathf.Lerp (currentMonster, monster, 1f * Time.deltaTime);
		bool monsterUp = currentMonster >= 0.99f;

		headMeshRenderer.gameObject.SetActive (true);
		comboverMeshRenderer.gameObject.SetActive (!monsterUp);
		comboverMeshRenderer.SetBlendShapeWeight (0, currentMonster*100.0f);
		comboverMaterial.SetFloat ("_Lerp", monster);
		monsterMeshRenderer.gameObject.SetActive (monsterUp);
		hairParticleSystem.gameObject.SetActive (monsterUp);

		rotoCam.minDistance = Mathf.Lerp (normalRotoCamDistance, monsterRotoCamDistance, currentMonster);

		transform.localScale = Vector3.one * Mathf.Lerp (normalSize, monsterSize, currentMonster);

		anim.SetFloat("Monster", monster);

		tweetShooter.enabled = currentMonster < 0.1f;

		rotoCam.offsetMultiplier = Mathf.Lerp (1.0f, monsterSize, currentMonster); 
	}

	void OnCollisionEnter(Collision collision)
	{
		if (currentMonster < 0.99f) return;


		GameObject bumpee = collision.gameObject;
		int layer = bumpee.layer;

		// tweet
		if (layer == 9)
		{
			EatTweet(bumpee);
		}
		// voter
		else if (layer == 11)
		{
			EatVoter(bumpee);
		}
	}

	void EatTweet(GameObject target)
	{
		target.SetActive (false);
	}

	void EatVoter(GameObject target)
	{
		VoterController voter = target.GetComponent<VoterController>();
		voter.enabled = false;
		voter.GetComponent<CharacterMover>().enabled = false;
		voter.GetComponentInChildren<MeshRenderer>().sharedMaterial = eatenVoterMaterial;
		voter.GetComponent<SphereCollider>().enabled = false;
		Rigidbody vrb = voter.GetComponent<Rigidbody>();
		RigidbodyConstraints oldConstraints = vrb.constraints;
		vrb.constraints = RigidbodyConstraints.None;

		StartCoroutine(WakeVoter(voter, vrb, oldConstraints));
	}

	IEnumerator WakeVoter(VoterController voter, Rigidbody vrb, RigidbodyConstraints constraints)
	{
		yield return new WaitForSeconds(12.0f);

		vrb.constraints = constraints;

		// set to blank voter
		VoterSpawnGroup group = VoterSpawner.instance.FindVoterSpawnGroup("un");
		voter.GetComponentInChildren<MeshRenderer>().sharedMaterial = group.candidateMaterial;
		voter.tweetKey = "un";

		voter.GetComponent<CharacterMover>().enabled = true;
		voter.enabled = true;
		voter.GetComponent<SphereCollider>().enabled = true;
		Vector3 pos = vrb.position;
		pos.y = 0.0f;
		vrb.transform.position = pos;
		vrb.transform.rotation = Quaternion.identity * Quaternion.Euler (0.0f, Random.value * 360.0f, 0.0f);
	}
}
