﻿using UnityEngine;
using System.Collections;

public class InvisibleWall : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnCollisionStay(Collision collision)
	{
		Rigidbody otherRB = collision.rigidbody;
		otherRB.position = Vector3.MoveTowards (otherRB.position, Vector3.zero,20.0f);
	}
}
