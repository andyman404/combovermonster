﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(CharacterMover))]
public class PlayerController : MonoBehaviour {

	private CharacterMover mover;

	// Use this for initialization
	void Start () {
		mover = GetComponent<CharacterMover>();
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		Vector2 inputs = new Vector2(
			Input.GetAxis ("Horizontal"),
			Input.GetAxis ("Vertical"));

		float push = inputs.magnitude;

		Quaternion camRot = Camera.main.transform.rotation;
		// strip out the y
		Vector3 camForward = camRot * Vector3.forward;
		camForward.y = 0.0f;
		camForward.Normalize();
		camRot = Quaternion.LookRotation (camForward);

		Vector3 dir = 
				camRot * Vector3.forward * inputs.y
				+ camRot * Vector3.right * inputs.x;

		mover.Move (dir, push);

	}


}
