﻿using UnityEngine;
using System.Collections;

public class TweetShooter : MonoBehaviour {

	public TweetController tweetPrefab;
	public string key = "dt";

	public Transform spawnPoint;
	public float tweetStartSpeed = 10.0f;
	public float tweetInterval = 0.5f;

	public TweetCanvas tweetCanvas;

	public Color tweetColor = Color.white;
	public bool playerControlled = false;
	private TweetGrabber grabber;
	private float nextTweetTime;

	// Use this for initialization
	void Start () {
		grabber = TweetGrabber.instance;
		if (tweetCanvas == null)
		{
			tweetCanvas = TweetCanvas.instance;
		}
	}
	
	// Update is called once per frame
	void Update () {
	
		if (playerControlled && Time.time > nextTweetTime && Input.GetButton("Fire1"))
		{
			FireTweet(key);
			nextTweetTime = Time.time + tweetInterval;
		}
	}

	public void FireTweet(string key)
	{
		VoterSpawnGroup group = VoterSpawner.instance.FindVoterSpawnGroup(key);

		string msg = "<color=white><i>" + group.tweetHandle + "</i>:</color>\n"
			+ grabber.GetNextTweet (key);

		TweetController tweet = tweetCanvas.CreateNewTweet (msg, spawnPoint.position, spawnPoint.rotation);
		tweet.SetTweetColor (tweetColor);
		tweet.source = this;
		tweet.gameObject.SetActive (true);
		Rigidbody tweetRB = tweet.GetComponent<Rigidbody>();

		tweetRB.velocity = spawnPoint.forward * tweetStartSpeed + tweetRB.velocity*2.0f;
	}

}
