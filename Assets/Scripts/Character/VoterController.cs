﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(CharacterMover))]
[RequireComponent(typeof(Rigidbody))]
public class VoterController : MonoBehaviour {
	public static List<VoterController> instances = new List<VoterController>();

	public string tweetKey;

	public float stopDist = 3.0f;

	public AudioClip changeSound;

	private Vector3 targetPosition;
	private Vector3 moveDir;
	private float push;
	private float nextCheckTime = 0.0f;
	private Vector2 checkInterval = new Vector2(3.0f, 6.0f);
	private Rigidbody rb;
	private CharacterMover mover;

	void Awake() {
		mover = GetComponent<CharacterMover>();
		rb = GetComponent<Rigidbody>();
	}
	// Use this for initialization
	void Start () {
	
	}
	public void PlayChangeSound()
	{
		AudioSource.PlayClipAtPoint (changeSound, transform.position, 1.0f);
	}

	// Update is called once per frame
	void Update () {
		if (Time.time > nextCheckTime)
		{
			nextCheckTime = Time.time + Random.Range (checkInterval.x, checkInterval.y);
			targetPosition = transform.position 
				+ new Vector3(
					Random.Range (-20.0f, 20.0f),
					0.0f,
					Random.Range (-20.0f, 20.0f));
		}

		Vector3 diff = targetPosition - transform.position;
		float dist = diff.magnitude;
		moveDir = diff.normalized;
		moveDir.y = 0.0f;
		moveDir.Normalize();

		push = dist > stopDist ? 1.0f : 0.0f;

		Update2();
	}

	void Update2() {
		if (mover != null && rb != null)
		{
			mover.Move (moveDir, push);
		}
		Vector3 pos = rb.position;
		pos.y = 0.0f;
		rb.transform.position = pos;
	}

	void OnEnable()
	{
		if (!instances.Contains (this))
		{
			instances.Add (this);
		}
	}

	void OnDisable()
	{
		if (instances.Contains (this))
		{
			instances.Remove (this);
		}
	}
}
