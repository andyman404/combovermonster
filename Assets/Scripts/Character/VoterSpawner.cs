﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class VoterSpawnGroup
{
	public string tweetKey;
	public string tweetHandle;
	public string candidateName;
	public VoterController voterPrefab;
	public Material candidateMaterial;
	public float votePercentage;
	public ParticleSystem particleSystem;
	public int party = 0; // 1 repub, 2 dem
	public int rank = 0;
	public int rankInParty = 0;
	public int pollCount = 0;
	public float partyPollPercentage = 0.0f;
	public void Emit(Vector3 position, int particleCount)
	{
		if (particleSystem != null)
		{
			particleSystem.transform.position = position;
			particleSystem.Emit (particleCount);
		}

	}
	
}

public class VoterSpawner : MonoBehaviour {
	public static VoterSpawner instance;

	public Transform voterContainer;

	public VoterSpawnGroup[] voterSpawns;

	public int voterCount = 10;


	public int lastVoterCount = 10;
	public float tallyInterval = 0.5f;

	private float nextTallyTime = 0.0f;
	private Dictionary<string, VoterSpawnGroup> groupMap;


	void OnEnable()
	{
		instance = this;
	}

	void OnDisable()
	{
		if (instance == this)
		{
			instance = null;
		}
	}
	void Awake()
	{
		groupMap = new Dictionary<string, VoterSpawnGroup>();
		for(int i = 0; i < voterSpawns.Length; i++)
		{
			groupMap.Add (voterSpawns[i].tweetKey, voterSpawns[i]);
		}
	}

	// Use this for initialization
	void Start () {
		SpawnVoters();
	}
	
	// Update is called once per frame
	void Update () {
		if (Time.time >= nextTallyTime)
		{
			Tally();
			nextTallyTime = Time.time + tallyInterval;
		}
	}

	public VoterSpawnGroup FindVoterSpawnGroup(string tweetKey)
	{
		if (groupMap.ContainsKey (tweetKey))
		{
			return groupMap[tweetKey];
		}
		return null;
	}

	public void Tally()
	{
		// reset all the tallies
		for(int i = 0; i < voterSpawns.Length; i++)
		{
			voterSpawns[i].pollCount = 0;
		}

		List<VoterController> voters = VoterController.instances;
		int childCount = voters.Count;
		lastVoterCount = 0;

		for(int i = 0; i < childCount; i++)
		{
			VoterController voter = voters[i];
			if (voter == null) continue;

			VoterSpawnGroup group = FindVoterSpawnGroup(voter.tweetKey);
			if (group == null) continue;

			group.pollCount++;
			lastVoterCount++;
		}
		System.Array.Sort(voterSpawns, delegate(VoterSpawnGroup group1, VoterSpawnGroup group2) {
			return group2.pollCount.CompareTo(group1.pollCount);
		});

		int[] partyRanks = {0,0,0};
		int[] partyTotals = {0,0,0};

		for(int i = 0; i < voterSpawns.Length; i++)
		{
			voterSpawns[i].rank = i;
			voterSpawns[i].rankInParty = partyRanks[voterSpawns[i].party]++;
			partyTotals[voterSpawns[i].party] += voterSpawns[i].pollCount;
		}

		for(int i = 0; i < voterSpawns.Length; i++)
		{
			if(partyTotals[voterSpawns[i].party] > 0)
			{
				voterSpawns[i].partyPollPercentage = ((float)voterSpawns[i].pollCount) / ((float)partyTotals[voterSpawns[i].party]);
			}
			else
			{
				voterSpawns[i].partyPollPercentage = 0.0f;
			}
		}

	}
	void SpawnVoters()
	{
		for(int i = 0; i < voterCount; i++)
		{
			VoterSpawnGroup group = PickSpinner();
			if (group == null) continue;

			Vector3 pos = new Vector3(
				Random.Range (-90.0f, 90.0f),
				0.0f,
				Random.Range (-90.0f, 90.0f)
				);

			Quaternion rot = Quaternion.identity;
			VoterController vc = Instantiate(group.voterPrefab, pos, rot) as VoterController;
			vc.tweetKey = group.tweetKey;
			vc.GetComponentInChildren<MeshRenderer>().sharedMaterial = group.candidateMaterial;
			vc.transform.SetParent (voterContainer);
		}
	}

	public VoterSpawnGroup PickSpinner()
	{
		float percentage = Random.value * 2.0f;
		float cumulative = 0.0f;
		for(int i=0; i < voterSpawns.Length; i++)
		{
			cumulative += voterSpawns[i].votePercentage;
			if (percentage <= cumulative)
			{
				return voterSpawns[i];
			}
		}

		return null;
	}
}
