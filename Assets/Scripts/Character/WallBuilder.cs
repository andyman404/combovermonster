﻿using UnityEngine;
using System.Collections;

public class WallBuilder : MonoBehaviour {
	public WallController wallPrefab;
	public LayerMask wallLayerMask;

	public GameObject wallUI;
	public GameObject tooCloseUI;

	public ParticleSystem wallDamageParticleSystem;
	public AudioClip wallSound;

	public static ParticleSystem sharedWallDamageParticleSystem;

	// Use this for initialization
	void Start () {
	}

	void OnEnable()
	{
		sharedWallDamageParticleSystem = wallDamageParticleSystem;
	}

	// Update is called once per frame
	void Update () {
		Vector3 position = transform.position + transform.right*3.0f;
		position.y = 0.0f;

		bool tooClose = (Physics.CheckSphere (position, 5.0f, wallLayerMask));
		wallUI.SetActive (!tooClose);
		tooCloseUI.SetActive (tooClose);

		if (!tooClose && (Input.GetKey (KeyCode.E) || Input.GetKey (KeyCode.Return)))
		{
			WallController wallController = Instantiate(wallPrefab, position, transform.rotation) as WallController;
			wallController.damageParticleSystem = wallDamageParticleSystem;

			AudioSource.PlayClipAtPoint (wallSound, position);
		}


	}


}
