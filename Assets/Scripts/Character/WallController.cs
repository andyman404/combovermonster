﻿using UnityEngine;
using System.Collections;

public class WallController : MonoBehaviour {
	public float maxLife = 10.0f;
	public float life = 10.0f;
	public float donaldMonsterDamage = 10.0f;
	public float voterDamage = 1.0f;
	public float candidateDamage = 3.0f;

	public AudioClip wallDieSound;
	public ParticleSystem damageParticleSystem;
	public float emitPerDamage = 10.0f;
	// Use this for initialization
	void Start () {
	
		if (damageParticleSystem == null)
		{
			damageParticleSystem = WallBuilder.sharedWallDamageParticleSystem;
		}
	}
	
//	// Update is called once per frame
//	void Update () {
//
//	}

	void Die()
	{
		if (damageParticleSystem != null)
		{
			damageParticleSystem.transform.position = transform.position + Vector3.up;
			damageParticleSystem.Emit (Mathf.RoundToInt (maxLife * emitPerDamage * 0.1f));
		}

		AudioSource audioSource = GetComponent<AudioSource>();
		audioSource.enabled = true;
		audioSource.PlayOneShot (wallDieSound);
		GetComponent<MeshRenderer>().enabled = false;
		Destroy(gameObject, 1.0f);
		this.enabled = false;
	}
	float damageQueue = 0.0f;
	void OnCollisionStay(Collision collision) {
		if (!this.enabled) return;

		GameObject target = collision.gameObject;
		int layer = target.layer;
		
		float damage = 0.0f;
		
		switch(layer)
		{
		case 8: // donald
			damage = target.GetComponent<DonaldTransformer>().monster * donaldMonsterDamage;
			break;
			
		case 11: // voter
			damage = voterDamage;
			break;
			
		case 13: // candidate
			damage = candidateDamage;
			break;
		}
		
		if (damage > 0.0f)
		{
			damage = Time.deltaTime * damage;
			damage = (damage > life) ? life : damage;
			life -= damage;

			damageQueue += damage * emitPerDamage;

			Debug.Log ("Wall damage: " + damage);
			if (damageParticleSystem != null && damageQueue > 1.0f)
			{
				int particles = Mathf.FloorToInt (damageQueue);
				damageQueue -= (float) particles;
				damageParticleSystem.transform.position = collision.contacts[0].point;

				damageParticleSystem.Emit (particles);
			}

			if (life <= 0.0f)
			{
				Die();
			}
		}
		
	}
}
