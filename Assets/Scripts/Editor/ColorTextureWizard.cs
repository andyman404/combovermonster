﻿using UnityEngine;
using UnityEditor;
using System.IO;

public class ColorTextureWizard : ScriptableWizard {
	public Color color = Color.white;
	public int size = 1024;

	[MenuItem("Assets/Color Texture Wizard")]
	static void CreateWizard() {
		// Clear progress bar
		EditorUtility.ClearProgressBar();

		ScriptableWizard.DisplayWizard<ColorTextureWizard>("Color Texture Wizard", "Create");

	}

	void OnWizardCreate () {
		string path = AssetDatabase.GetAssetPath (Selection.activeObject);
		if (Directory.Exists (path))
		{

		}
		else
		{
			path = Path.GetDirectoryName (path);
		}

		Texture2D tex = new Texture2D(size, size);
		Color[] pixels = tex.GetPixels();

		int pixelCount = pixels.Length;

		for(int i = 0; i < pixelCount; i++)
		{
			pixels[i] = color;
		}
		tex.SetPixels (pixels);
		tex.Apply ();

		byte[] bytes = tex.EncodeToPNG ();
		string filePath = path + "/color_texture.png";
		System.IO.File.WriteAllBytes(filePath, bytes);

		DestroyImmediate(tex);

		AssetDatabase.Refresh ();
	}

	static bool MakeTextureReadable(Texture2D texture, bool isReadable)
	{
		if (texture == null)
		{
			return isReadable;
		}
		string path = AssetDatabase.GetAssetPath (texture);
		if (path == null) 
		{
			Debug.Log ("Path not found for texture: " + texture.name);
			return isReadable;
		}
		TextureImporter textureImporter = TextureImporter.GetAtPath(path) as TextureImporter;
		if (textureImporter == null)
		{
			Debug.Log ("Texture importer not found for texture at: " + path);
			return isReadable;
		}
		bool oldValue = textureImporter.isReadable;
		textureImporter.isReadable = isReadable;
		AssetDatabase.ImportAsset (path);
		return oldValue;
	}

	void ClearWithColor32(Texture2D texture, Color32 color32)
	{
		Color32[] c = texture.GetPixels32 ();
		int cLength = c.Length;
		for(int i = 0; i < cLength; i++)
		{
			c[i] = color32;
		}
		texture.SetPixels32 (c);
	}
	
}
