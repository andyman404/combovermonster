﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.Serialization;

public class LevelController : MonoBehaviour {

	public float winStartedTime;
	public float winningDuration;
	public float remainingWinningDuration;

	public float minPartyMeter;

	public bool isWinning = false;
	public string playerTweetKey = "dt";
	public float winThreshold = 0.5f;
	public float minVictoryWinDuration = 30.0f;

	public float minLossThreshold = 13.0f;
	public float minLossWarningThreshold = 17.0f;

	public Text lossWarningUI;
	public Text winningUI;

	public bool won = false;
	public bool loss = false;

	private VoterSpawnGroup group = null;
	private float updatesOnlyAfterTime;
	/** Events to invoke after the delay */
	[SerializeField]
	public UnityEvent victoryEvent;

	/** Events to invoke after the delay */
	[SerializeField]
	public UnityEvent lossEvent;

	void Awake()
	{

		updatesOnlyAfterTime = Time.time + 3.0f;
	}
	// Use this for initialization
	void Start () {
		 
	}
	
	// Update is called once per frame
	void Update () {
		if (Time.time < updatesOnlyAfterTime)
		{
			return;
		}

		if (Input.GetKeyDown (KeyCode.Escape))
		{
			if (Application.isWebPlayer)
			{
				Time.timeScale = 1.0f;
				Application.LoadLevel (0);
				return;
			}
			else
			{
				Application.Quit ();
				return;
			}
		}

		if (Input.GetKeyDown (KeyCode.R))
		{
			Time.timeScale = 1.0f;
			Application.LoadLevel (Application.loadedLevel);
			return;
		}

		if (group == null)
		{
			group = VoterSpawner.instance.FindVoterSpawnGroup(playerTweetKey);
		}

		float percentage = group.partyPollPercentage;
		bool newIsWinning = percentage >= winThreshold;

		if (isWinning && !newIsWinning)
		{
			Debug.Log ("Lost the winning");
		}
		else if (!isWinning && newIsWinning)
		{
			winStartedTime = Time.time;
		}

		isWinning = newIsWinning;

		if (isWinning)
		{
			winningDuration = Time.time - winStartedTime;
			remainingWinningDuration = minVictoryWinDuration - winningDuration;
			float remainingRounded = Mathf.Round (remainingWinningDuration * 10.0f) / 10.0f;
			winningUI.enabled = true;
			winningUI.text = "Maintain 50% or higher rating for " + remainingRounded + " more seconds to for victory!";

			if (winningDuration >= minVictoryWinDuration)
			{
				Victory();
			}
		}

		if (percentage <= minLossWarningThreshold)
		{
			float distance = Mathf.Round ((percentage - minLossThreshold) * 1000.0f) / 10.0f;
			lossWarningUI.enabled = true;
			if (distance > 0)
			{
				lossWarningUI.text = "WARNING: You will lose if your ratings drop " + distance + "% lower.";
			}
			else
			{
				lossWarningUI.text = "YOU LOSE!";
				Loss();
			}

			winningUI.enabled = false;
		}
		else if (!isWinning)
		{
			lossWarningUI.enabled = false;
			float distance = Mathf.Round((winThreshold - percentage) * 1000.0f) / 10.0f;
			winningUI.enabled = true;
			winningUI.text = "You need " + distance + "% higher ratings to attemp victory.";
		}
		else
		{
			lossWarningUI.enabled = false;
		}

	}


	public void Victory()
	{
		won = true;
		Time.timeScale = 0.0f;
		Debug.Log ("Victory");
		victoryEvent.Invoke();
	}

	public void Loss()
	{
		loss = true;
		Time.timeScale = 0.0f;
		Debug.Log ("Loss");
		lossEvent.Invoke ();
	}
}
