﻿using UnityEngine;
using System.Collections;

public class TweetCanvas : MonoBehaviour {
	public static TweetCanvas instance;

	public TweetController tweetPrefab;
	public const int MAX_TWEETS = 64;
	void OnEnable()
	{
		instance = this;
	}
	void OnDisable()
	{
		if (instance == this)
		{
			instance = null;
		}
	}

	void Awake()
	{
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public TweetController CreateNewTweet(string message, Vector3 position, Quaternion rotation)
	{
		TweetController tweet = null;

		int children = transform.childCount;
		for(int i = 0; i < children; i++)
		{
			Transform child = transform.GetChild (i);
			if (!child.gameObject.activeSelf)
			{
				tweet = child.GetComponent<TweetController>();
				if (tweet != null)
				{
					break;
				}
			}
		}

		if (tweet == null)
		{
			if (children < MAX_TWEETS-1)
			{
				tweet = Instantiate(tweetPrefab, position, rotation) as TweetController;
				tweet.transform.SetParent (transform);
				tweet.message = message;
			}
			else
			{
				Debug.Log ("Forced reuse");
				tweet = transform.GetChild (Random.Range (0, children)).GetComponent<TweetController>();
				tweet.transform.position = position;
				tweet.transform.rotation = rotation;
				tweet.message = message;
				tweet.ResetTime();
			}
		}
		else
		{
			tweet.transform.position = position;
			tweet.transform.rotation = rotation;
			tweet.message = message;
			tweet.ResetTime();
		}

		return tweet;
	}
}
