﻿using UnityEngine;
using UnityEngine.UI;

using System.Collections;

public class TweetController : MonoBehaviour {
	
	public string message;
	public string oldMessage;
	public Text[] texts;
	public float duration = 8.0f;

	public TweetShooter source;

	private float endTime;
	private float startTime;
	// Use this for initialization
	void Start () {
		oldMessage = message;
		ResetTime();
	}
	public void ResetTime()
	{
		startTime = Time.time;
		endTime = startTime + duration;
		transform.localScale = Vector3.one * 0.1f;
		UpdateTexts();

	}
	public void SetTweetColor(Color color)
	{
		for(int i = 0; i < texts.Length; i++)
		{
			texts[i].color = color;
		}
	}
	// Update is called once per frame
	void Update () {
		if (message != oldMessage)
		{
			UpdateTexts();
			oldMessage = message;
		}

		if (Time.time > endTime)
		{
			gameObject.SetActive (false);
		}

		float timeLerp = Mathf.InverseLerp (startTime, endTime, Time.time);
		if (timeLerp < 0.1f)
		{
			transform.localScale = Vector3.one * (timeLerp/0.1f + 0.1f) ;
		}
		else if (timeLerp < 0.9f)
		{
			transform.localScale = Vector3.one * 1.1f;
		}
		else
		{
			float scale = 1.1f - ((timeLerp - 0.9f) / 0.1f);
			transform.localScale = Vector3.one * scale;
		}
	}

	void UpdateTexts()
	{
		for(int i = 0; i < texts.Length; i++)
		{
			texts[i].text = message;
		}
	}

	void OnCollisionEnter(Collision collision)
	{
		// voter layer
		if (collision.gameObject.layer == 11)
		{
			// check if it is the same key
			VoterController voter = collision.gameObject.GetComponent<VoterController>();
			if (voter == null || !voter.enabled) return;

			// already the converted
			if (voter.tweetKey == source.key) return;
			VoterSpawnGroup oldGroup = VoterSpawner.instance.FindVoterSpawnGroup(voter.tweetKey);
			VoterSpawnGroup group = VoterSpawner.instance.FindVoterSpawnGroup (source.key);

			if (oldGroup.party == 0 || oldGroup.party == group.party)
			{
				voter.tweetKey = source.key;
				voter.GetComponentInChildren<MeshRenderer>().sharedMaterial = group.candidateMaterial;
				group.Emit (collision.contacts[0].point, 1);
			}
			endTime -= 2.0f;

		}
	}
}
