﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class TweetFetch {
	public string key;
	public string url;
	public string[] defaultTweets;
}
public class TweetGrabber : MonoBehaviour {
	public static TweetGrabber instance;

	public string tweetURL = "http://ld33.idumpling.com/tweets_dt.txt";

	public Dictionary<string,string[]> tweetMap;

	[SerializeField]
	public TweetFetch[] fetches;

	private int nextIndex = 0;

	void Awake() {
		tweetMap = new Dictionary<string, string[]>();
	}

	// Use this for initialization
	void Start () {
		for(int i = 0; i < fetches.Length; i++)
		{
			tweetMap.Add (fetches[i].key, fetches[i].defaultTweets);
			StartCoroutine(Grab(fetches[i]));
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public string GetNextTweet(string key)
	{
		string[] tweets = tweetMap[key];
		string tweet = tweets[nextIndex % tweets.Length];
		nextIndex++;
		return tweet;
	}

	IEnumerator Grab(TweetFetch fetch) {
		WWW request = new WWW(fetch.url);
		yield return request;

		string[] tweets = request.text.Split (new string[]{"\n"}, System.StringSplitOptions.RemoveEmptyEntries);
		fetch.defaultTweets = tweets;
		tweetMap[fetch.key] = tweets;
//		Debug.Log ("Fetched " + fetch.key + " (" + tweets.Length + ")");
	}

	void OnEnable()
	{
		instance = this;
	}
	void OnDisable()
	{
		if (instance == this)
		{
			instance = null;
		}
	}

}
