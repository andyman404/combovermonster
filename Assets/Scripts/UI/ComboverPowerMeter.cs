﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class ComboverPowerMeter : MonoBehaviour {

	Slider slider;
	public DonaldTransformer donaldTransformer;
	public ParticleSystem sliderParticleSystem;
	public Text holdShiftLabel;
	// Use this for initialization
	void Start () {
		slider = GetComponent<Slider>();
	}
	
	// Update is called once per frame
	void Update () {
	
		slider.value = donaldTransformer.comboverPower / donaldTransformer.maxComboverPower;
		sliderParticleSystem.emissionRate = slider.value * 20.0f;

		holdShiftLabel.enabled = (slider.value > 0.3f);
	}
}
