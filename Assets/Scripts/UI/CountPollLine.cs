﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CountPollLine : MonoBehaviour {

	public string tweetKey;

	public int oldCount = -1000;
	Text text;

	// Use this for initialization
	void Start () {
		text = GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update () {
		VoterSpawnGroup group = VoterSpawner.instance.FindVoterSpawnGroup (tweetKey);

		if (oldCount != group.pollCount)
		{
			text.text = group.pollCount.ToString();
			oldCount = group.pollCount;
		}

	}
}
