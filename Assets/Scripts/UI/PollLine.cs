﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PollLine : MonoBehaviour {

	public float heightPerRow = 25.0f;
	public string tweetKey;

	public Slider slider;
	public Text nameLabel;
	public Text percentageLabel;

	public float oldPercentage = -1000f;
	public int oldRank = -1000;

	RectTransform rectTransform;
	// Use this for initialization
	void Start () {
		rectTransform = GetComponent<RectTransform>();
	}
	
	// Update is called once per frame
	void Update () {
		VoterSpawnGroup group = VoterSpawner.instance.FindVoterSpawnGroup (tweetKey);

		if (oldRank != group.rankInParty)
		{
			Vector3 pos = rectTransform.localPosition;
			pos.y = -group.rankInParty * heightPerRow;
			rectTransform.localPosition = pos;
			oldRank = group.rankInParty;
		}

		if (group.partyPollPercentage != oldPercentage)
		{
			slider.value = group.partyPollPercentage;
			percentageLabel.text = (Mathf.Round(group.partyPollPercentage * 1000.0f)/10.0f).ToString() + "% "
				+ "(" + group.pollCount + ")";
			oldPercentage = group.partyPollPercentage;
		}

	}
}
