﻿Shader "Custom/DonaldHair" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_MainTex2 ("Albedo2 (RGB)", 2D) = "white" {}
		_Lerp("Lerp", Range(0,1)) = 0.0
		
		_BumpMap("Normal Map", 2D) = "bump" {}
		_BumpMap2("Normal Map2", 2D) = "bump" {}
		
		_Glossiness ("Smoothness", Range(0,1)) = 0.5
		_Metallic ("Metallic", Range(0,1)) = 0.0
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf Standard fullforwardshadows

		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.0

		sampler2D _MainTex;
		sampler2D _MainTex2;
		sampler2D _BumpMap;
		sampler2D _BumpMap2;

		struct Input {
			float2 uv_MainTex;
		};

		float _Lerp;
		
		half _Glossiness;
		half _Metallic;
		fixed4 _Color;

		void surf (Input IN, inout SurfaceOutputStandard o) {
			// Albedo comes from a texture tinted by color
			fixed4 c0 = tex2D (_MainTex, IN.uv_MainTex) * _Color;
			fixed4 c1 = tex2D (_MainTex2, IN.uv_MainTex) * _Color;
			fixed4 c = lerp(c0, c1, _Lerp);
			o.Albedo = c.rgb;
			// Metallic and smoothness come from slider variables
			o.Metallic = _Metallic;
			o.Smoothness = _Glossiness;
			
			fixed3 n0 = UnpackNormal(tex2D(_BumpMap, IN.uv_MainTex));
			fixed3 n1 = UnpackNormal(tex2D(_BumpMap2, IN.uv_MainTex));
			
			fixed3 n = lerp(n0, n1, _Lerp);
			o.Normal = n;
			o.Alpha = c.a;
		}
		ENDCG
	} 
	FallBack "Diffuse"
}
