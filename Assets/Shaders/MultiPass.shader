﻿Shader "Tutorial/Textured Colored" {
    Properties {
        _Color ("Main Color", Color) = (1,1,1,0.5)
        _BorderColor("Border Color", Color) = (1,1,1,1)
        _BorderThickness("Border Thickness", Float) = 0.01
        _MainTex ("Texture", 2D) = "white" { }
    }
    SubShader {
        Tags { "Queue" = "AlphaTest" }
        // base
        Pass {
        	Blend One Zero
        	Cull Off
	        CGPROGRAM
	        #pragma vertex vert
	        #pragma fragment frag

	        #include "UnityCG.cginc"

	        fixed4 _Color;
	        float _BorderThickness;
	        sampler2D _MainTex;

	        struct v2f {
	            float4 pos : SV_POSITION;
	            float2 uv : TEXCOORD0;
	        };

	        float4 _MainTex_ST;

	        v2f vert (appdata_base v)
	        {
	            v2f o;
	            o.pos = mul (UNITY_MATRIX_MVP, v.vertex);
	            o.uv = TRANSFORM_TEX (v.texcoord, _MainTex);
	            return o;
	        }

	        fixed4 frag (v2f i) : SV_Target
	        {
	            fixed4 texcol = tex2D (_MainTex, i.uv);
	            return texcol * _Color;
	        }
	        ENDCG

        }
        
        // grown
        Pass {
        	Blend OneMinusDstAlpha DstAlpha
        	Cull Off
//        	ZTest Always
        	Offset 0,-1000
	        CGPROGRAM
	        #pragma vertex vert
	        #pragma fragment frag

	        #include "UnityCG.cginc"

	        fixed4 _BorderColor;
			float _BorderThickness;
			
	        struct v2f {
	            float4 pos : SV_POSITION;
	        };

	        float4 _MainTex_ST;

	        v2f vert (appdata_base v)
	        {
	            v2f o;
	            float3 localOffset = v.normal * _BorderThickness;
				o.pos = mul(UNITY_MATRIX_MVP, v.vertex + float4(localOffset,0.0));
	            
//				float3 norm   = normalize(mul ((float3x3)UNITY_MATRIX_IT_MV, v.normal));
//				float2 offset = TransformViewToProjection(norm.xy);
//
//				o.pos.xy += offset * o.pos.z * _BorderThickness;
	 
	                       
	            return o;
	        }

	        fixed4 frag (v2f i) : SV_Target
	        {
	            return _BorderColor;
	        }
	        ENDCG

        }
        
    }
}
