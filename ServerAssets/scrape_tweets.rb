require 'nokogiri'
require 'open-uri'

out_path = "/home/andyman/public_html/ld33/"
urlToFile = {
  "https://twitter.com/realDonaldTrump"=>"tweets_dt.txt",
  "https://twitter.com/JebBush"=>"tweets_jb.txt",
  "https://twitter.com/RealBenCarson"=>"tweets_bc.txt",
  "https://twitter.com/ScottWalker"=>"tweets_sc.txt",
  "https://twitter.com/marcorubio"=>"tweets_mr.txt",
  "https://twitter.com/tedcruz"=>"tweets_tc.txt",
  "https://twitter.com/CarlyFiorina"=>"tweets_cf.txt",
  "https://twitter.com/RandPaul"=>"tweets_rp.txt",
  "https://twitter.com/GovMikeHuckabee"=>"tweets_mh.txt",
  "https://twitter.com/ChrisChristie"=>"tweets_cc.txt",
  "https://twitter.com/HillaryClinton"=>"tweets_hc.txt",
  "https://twitter.com/berniesanders"=>"tweets_bs.txt",
  "https://twitter.com/MartinOMalley"=>"tweets_mm.txt"
}

urlToFile.each_pair { |url, filename|
  page = Nokogiri::HTML(open(url))
  tweets = page.css(".tweet-text").collect {|t| t.text}
    open(out_path+filename, 'w') { |f|
      tweets.each {|t| f.puts t}
    }
}

